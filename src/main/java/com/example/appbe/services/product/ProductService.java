package com.example.appbe.services.product;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.util.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.dto.CreateProductDto;
import com.example.appbe.dto.UpdateProductDto;
import com.example.appbe.interfaces.ProductInterface;
import com.example.appbe.model.Product;
import com.example.appbe.model.ProductDetail;
import com.example.appbe.model.ProductType;
import com.example.appbe.repositories.ProductRepository;
import com.example.appbe.services.category.CategoryService;
import com.example.appbe.services.product_detail.ProductDetailService;
import com.example.appbe.services.product_type.ProductTypeService;
import com.example.appbe.utils.FileUploadUtil;

@Service
public class ProductService implements ProductInterface {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductTypeService productTypeService;

    @Override
    public Product createProduct(CreateProductDto createProductDto) throws Exception {
        // TODO Auto-generated method stub
        String fileName = StringUtils.cleanPath(createProductDto.getProductImage().getOriginalFilename());
        String fileCode = RandomStringUtils.randomAlphanumeric(8);
        FileUploadUtil.saveFile(fileCode + "-" + fileName, createProductDto.getProductImage());
        Product product = new Product(null, fileCode + "-" + fileName,
                createProductDto.getProductName(), createProductDto.getPrice(),
                createProductDto.getQuantity(), 0, createProductDto.getDescription(),
                createProductDto.getDiscount(), categoryService.findCategoryById(createProductDto.getCategoryId()),
                productTypeService.findProductTypeById(createProductDto.getProductTypeId()), new Date());
        // System.out.println(product);
        productRepository.save(product);
        return product;
    }

    @Override
    public List<Product> getAllProducts() {
        // TODO Auto-generated method stub
        List<Product> listP = productRepository.findAll();
        return listP;
    }

    @Override
    public List<Product> getProductByFilter(Integer categoryId, Integer productTypeId) {
        // TODO Auto-generated method stub
        List<Product> listProducts = productRepository.getProductByFilter(categoryId, productTypeId);
        return listProducts;
    }

    @Override
    public List<Product> getProductsByName(String productName) {
        // TODO Auto-generated method stub
        List<Product> listProducts = productRepository.getProductsByName(productName);
        return listProducts;
    }

    @Override
    public Product getProductById(Integer Id) {
        // TODO Auto-generated method stub
        Optional<Product> product = productRepository.findById(Id);
        return product.get();
    }

    @Override
    public void deleteProductById(Integer Id) {
        // TODO Auto-generated method stub
        Product product = productRepository.findById(Id).get();
        productRepository.delete(product);
    }

    @Override
    public Product updateProductById(UpdateProductDto updateProductDto) {
        // TODO Auto-generated method stub
        Product product = productRepository.findById(updateProductDto.getId()).orElse(null);
        if (product == null) {
            return null;
        }
        product.setProductName(updateProductDto.getProductName());
        product.setPrice(updateProductDto.getPrice());
        product.setQuantity(updateProductDto.getQuantity());
        product.setDescription(updateProductDto.getDescription());
        product.setDiscount(updateProductDto.getDiscount());
        product.setCategory(categoryService.findCategoryById(updateProductDto.getCategoryId()));
        product.setProductType(productTypeService.findProductTypeById(updateProductDto.getProductTypeId()));
        productRepository.save(product);
        return product;
    }

    @Override
    public Product updateProductById1(Integer id, Integer soLuongBan) {
        // TODO Auto-generated method stub
        Product product = productRepository.findById(id).get();
        product.setQuantity(product.getQuantity() - soLuongBan);
        product.setSold(product.getSold() + soLuongBan);
        productRepository.save(product);
        return product;
    }

    @Override
    public List<Product> getProductByIds(List<Integer> ids) {
        // TODO Auto-generated method stub
        List<Product> products = productRepository.findAllById(ids);
        return products;
    }
}
