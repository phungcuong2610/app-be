package com.example.appbe.services.cart;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.dto.CartDto;
import com.example.appbe.dto.UpdateCartDto;
import com.example.appbe.interfaces.CartInterface;
import com.example.appbe.model.Account;
import com.example.appbe.model.Bill;
import com.example.appbe.model.Cart;
import com.example.appbe.repositories.AccountRepository;
import com.example.appbe.repositories.BillRepository;
import com.example.appbe.repositories.CartRepository;

@Service
public class CartService implements CartInterface {
  @Autowired
  private CartRepository cartRepository;

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private BillRepository billRepository;

  @Override
  public Cart createCart(CartDto cardDto) {
    // TODO Auto-generated method stub
    Account account = accountRepository.findById(cardDto.getAccountId()).get();
    Cart cart = cartRepository
        .save(new Cart(null, cardDto.getProductId(), account, cardDto.getQuantity(), cardDto.getPrice(), false, null,
            null));
    return cart;
  }

  @Override
  public void deleteCart(Integer id) {
    // TODO Auto-generated method stub
    cartRepository.deletCart(id);
  }

  @Override
  public void deleteCartByIds(List<Integer> ids) {
    // TODO Auto-generated method stub
    cartRepository.deleteAllByIdInBatch(ids);
  }

  @Override
  public List<Cart> getCartByAccounId(Integer accountId, boolean isPay) {
    // TODO Auto-generated method stub
    List<Cart> carts = cartRepository.getAllCartByAccountId(accountId);
    List<Cart> lCarts = carts.stream().filter(item -> item.getIsPay() == isPay).collect(Collectors.toList());
    return lCarts;
  }

  @Override
  public Cart updateCart(UpdateCartDto updateCartDto) {
    // TODO Auto-generated method stub
    Cart cart = cartRepository.findById(updateCartDto.getId()).get();
    cart.setQuantity(updateCartDto.getQuantity());
    cart.setPrice(updateCartDto.getPrice());
    cart.setIsPay(updateCartDto.getIsPay());
    return cartRepository.save(cart);
  }

  @Override
  public List<Cart> getCartByIds(List<Integer> ids) {
    // TODO Auto-generated method stub
    return cartRepository.findAllById(ids);
  }

  @Override
  public Cart updateIsPay(Integer id, boolean isPay, Integer billId) {
    // TODO Auto-generated method stub
    Cart cart = cartRepository.findById(id).get();
    cart.setIsPay(isPay);
    cart.setBill(billRepository.findById(billId).get());
    return cartRepository.save(cart);
  }

  @Override
  public List<Cart> getCartByBillId(List<Integer> billIds) {
    // TODO Auto-generated method stub
    List<Cart> carts = cartRepository.getCartsByBillId(billIds);
    return carts;
  }

}
