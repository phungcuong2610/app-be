package com.example.appbe.services.bill;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.dto.CreateBillDto;
import com.example.appbe.interfaces.BillInterface;
import com.example.appbe.model.Bill;
import com.example.appbe.model.Cart;
import com.example.appbe.model.Product;
import com.example.appbe.repositories.BillRepository;
import com.example.appbe.services.account.AccountServiceAdaptee;
import com.example.appbe.services.cart.CartService;
import com.example.appbe.services.product.ProductService;

@Service
public class BillService implements BillInterface {
  @Autowired
  private CartService cartService;

  @Autowired
  private ProductService productService;

  @Autowired
  private BillRepository billRepository;

  @Autowired
  private AccountServiceAdaptee accountService;

  @Override
  public Bill createBill(CreateBillDto createBillDto) {
    // TODO Auto-generated method stub
    Bill newBill = billRepository.save(new Bill(null, createBillDto.getTotal(),
        accountService.getAccountById(createBillDto.getAccountId()), new Date()));
    List<Cart> carts = cartService.getCartByIds(createBillDto.getCartIds());
    System.out.println(carts);
    carts.forEach(
        item -> {
          System.out.println(item.getId());
          Cart cart = cartService.updateIsPay(item.getId(), true, newBill.getId());
          Product product = productService.updateProductById1(item.getProduct_id(),
              item.getQuantity());
          System.out.println(cart);
          System.out.println(product);
        });
    // System.out.println(carts);
    return newBill;
  }

  @Override
  public List<List<Object>> getBillByAccountId(Integer accountId) {
    // TODO Auto-generated method stub
    List<List<Object>> histories = billRepository.getHistoryAccountById(accountId);
    return histories;
  }

}
