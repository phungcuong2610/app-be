package com.example.appbe.services.product_type;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.interfaces.ProductTypeInterface;
import com.example.appbe.model.ProductType;
import com.example.appbe.repositories.ProductTypeRepository;

@Service
public class ProductTypeService implements ProductTypeInterface {
    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Override
    public ProductType findProductTypeById(Integer Id) {
        // TODO Auto-generated method stub
        Optional<ProductType> optProductType = productTypeRepository.findById(Id);
        return new ProductType(optProductType.get().getId(), optProductType.get().getNameType());
    }

    @Override
    public List<ProductType> getAllProductTypes() {
        // TODO Auto-generated method stub
        return null;
    }
}
