package com.example.appbe.services.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.appbe.model.Account;
import com.example.appbe.model.History;
import com.example.appbe.model.User;
import com.example.appbe.repositories.AccountRepository;
import com.example.appbe.repositories.UserRepository;
import com.example.appbe.services.history.HistoryService;

@Service
public class AccountServiceAdaptee implements IAccountService {
  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private HistoryService historyService;

  @Autowired
  private UserRepository userRepository;

  @Override
  public Account getAccountByUsername(String username) {
    // TODO Auto-generated method stub
    return accountRepository.findByUsername(username);
  }

  @Override
  public Account createAccount(Account account) {
    // TODO Auto-generated method stub
    Account acc = accountRepository.save(account);

    History his = historyService.createHistory(acc.getId());
    User user = userRepository.save(new User(null, "", "", "", "", account));
    return acc;
  }

  @Override
  public Account getAccountById(Integer id) {
    // TODO Auto-generated method stub
    Account account = accountRepository.findById(id).get();
    return account;
  }

}
