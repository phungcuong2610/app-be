package com.example.appbe.services.account;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.appbe.model.Account;

@Service
public class AccountService implements UserDetailsService {
  private final AccountServiceAdaptee accountServiceAdaptee;

  public AccountService(AccountServiceAdaptee accountServiceAdaptee) {
    this.accountServiceAdaptee = accountServiceAdaptee;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    // TODO Auto-generated method stub
    // Lấy ra thông tin của user theo username chứa các quyền, ....
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
    Account account = accountServiceAdaptee.getAccountByUsername(username);
    List<String> roles = new ArrayList<>();
    roles.add(bCryptPasswordEncoder.encode(account.getPassword()));
    UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
        .username(account.getUsername())
        .roles(roles.toArray(new String[0])).build();
    return userDetails;
  }

}
