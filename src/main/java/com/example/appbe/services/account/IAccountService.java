package com.example.appbe.services.account;

// import java.util.List;

import com.example.appbe.model.Account;

public interface IAccountService {
  Account getAccountByUsername(String username);

  Account createAccount(Account account);

  Account getAccountById(Integer id);
}
