package com.example.appbe.services.category;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.interfaces.CategoryInterface;
import com.example.appbe.model.Category;
import com.example.appbe.repositories.CategoryRepository;

@Service
public class CategoryService implements CategoryInterface {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category findCategoryById(Integer Id) {
        // TODO Auto-generated method stub
        Optional<Category> optCategory = categoryRepository.findById(Id);
        return new Category(optCategory.get().getId(), optCategory.get().getCategoryName());
    }

    @Override
    public List<Category> getAllCategory() {
        // TODO Auto-generated method stub
        return null;
    }

}
