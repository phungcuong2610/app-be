package com.example.appbe.services.user;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.appbe.dto.UpdateUserDto;
import com.example.appbe.interfaces.UserInterface;
import com.example.appbe.model.Account;
import com.example.appbe.model.User;
import com.example.appbe.repositories.UserRepository;
import com.example.appbe.services.account.AccountServiceAdaptee;
import com.example.appbe.utils.FileUploadUtil;

@Service
public class UserService implements UserInterface {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AccountServiceAdaptee accountServiceAdaptee;

  @Override
  public User getUserByAccountId(Integer accountId) {
    // TODO Auto-generated method stub
    User user = userRepository.getUserByAccountId(accountId);
    // System.out.println(user);
    if (user == null) {
      return null;
    }
    return user;
  }

  @Override
  public User updateUser(UpdateUserDto updateUserDto) throws Exception {
    // TODO Auto-generated method stub
    Account account = accountServiceAdaptee.getAccountByUsername(updateUserDto.getUsername());
    String fileName = StringUtils.cleanPath(updateUserDto.getAvatar().getOriginalFilename());
    String fileCode = RandomStringUtils.randomAlphanumeric(8);
    FileUploadUtil.saveFile(fileCode + "-" + fileName,
        updateUserDto.getAvatar());
    User user = userRepository.getUserByAccountId(account.getId());
    return userRepository.save(new User(user.getId(), fileCode + "-" +
        fileName,
        updateUserDto.getFullname(), updateUserDto.getPhone(),
        updateUserDto.getGmail(), account));

  }
}
