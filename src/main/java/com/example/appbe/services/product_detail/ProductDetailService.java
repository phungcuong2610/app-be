package com.example.appbe.services.product_detail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.dto.UpdateProductDetailDto;
import com.example.appbe.interfaces.ProductDetailInterface;
import com.example.appbe.model.Product;
import com.example.appbe.model.ProductDetail;
import com.example.appbe.repositories.ProductDetailRepository;
import com.example.appbe.services.product.ProductService;

@Service
public class ProductDetailService implements ProductDetailInterface {
    @Autowired
    private ProductDetailRepository productDetailRepository;

    @Autowired
    private ProductService productService;

    @Override
    public ProductDetail getProductDetailByProductId(Integer productId) {
        // TODO Auto-generated method stub
        return productDetailRepository.getProductDetailByProductId(productId);
    }

    @Override
    public ProductDetail updateProductDetail(UpdateProductDetailDto updateProductDetailDto) {
        Product product = productService.getProductById(updateProductDetailDto.getProductId());
        ProductDetail pDetail = productDetailRepository
                .getProductDetailByProductId(updateProductDetailDto.getProductId());
        if (pDetail == null) {
            ProductDetail productDetail = productDetailRepository
                    .save(new ProductDetail(null, updateProductDetailDto.getCpu(),
                            updateProductDetailDto.getRam(),
                            updateProductDetailDto.getOcung(), updateProductDetailDto.getHdh(),
                            product));
            return productDetail;
        } else {
            pDetail.setCpu(updateProductDetailDto.getCpu());
            pDetail.setRam(updateProductDetailDto.getRam());
            pDetail.setOcung(updateProductDetailDto.getOcung());
            pDetail.setHdh(updateProductDetailDto.getHdh());
            return productDetailRepository.save(pDetail);
        }
    }

    @Override
    public ProductDetail createProductDetail(Integer productId) {
        // TODO Auto-generated method stub
        return new ProductDetail(null, null, null, null, null, productService.getProductById(productId));
    }

    @Override
    public List<List<Object>> getProductDetialByPid(Integer pid) {
        // TODO Auto-generated method stub
        List<List<Object>> tmp = productDetailRepository.getProductDetailByPid(pid);
        return tmp;
    }

    @Override
    public void deleteProduct(Integer id) {
        // TODO Auto-generated method stub
        ProductDetail productDetail = productDetailRepository.getProductDetailByProductId(id);
        productDetailRepository.delete(productDetail);

    }

}
