package com.example.appbe.services.history;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.appbe.interfaces.HistoryInterface;
import com.example.appbe.model.Account;
import com.example.appbe.model.History;
import com.example.appbe.repositories.AccountRepository;
import com.example.appbe.repositories.HistoryRepository;

@Service
public class HistoryService implements HistoryInterface {
  @Autowired
  private HistoryRepository historyRepository;

  @Autowired
  private AccountRepository accountRepository;

  @Override
  public History createHistory(Integer account_id) {
    // TODO Auto-generated method stub
    Account account = accountRepository.findById(account_id).get();
    History history = historyRepository.save(new History(null, account));
    return history;
  }

  @Override
  public History getHistoryById(Integer id) {
    // TODO Auto-generated method stub
    History history = historyRepository.findById(id).get();
    return history;
  }

}
