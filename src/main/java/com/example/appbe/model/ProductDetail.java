package com.example.appbe.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product_detail")
public class ProductDetail {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Column(name = "cpu", nullable = false)
  private String cpu;

  @Column(name = "ram", nullable = false)
  private String ram;

  @Column(name = "ocung", nullable = false)
  private String ocung;

  @Column(name = "hdh", nullable = false)
  private String hdh;

  @OneToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "product_id", referencedColumnName = "id")
  private Product product;

}
