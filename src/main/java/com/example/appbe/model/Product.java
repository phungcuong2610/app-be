package com.example.appbe.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products")
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  private Integer id;

  @Column(name = "productImage", nullable = false)
  private String productImage;

  @Column(name = "productName", nullable = false)
  private String productName;

  @Column(name = "price", nullable = false)
  private Double price;

  @Column(name = "quantity", nullable = false)
  private Integer quantity;

  @Column(name = "sold", nullable = false)
  private Integer sold;

  @Column(name = "description", columnDefinition = "text", nullable = true)
  private String description;

  @Column(name = "discount", nullable = false)
  private Double discount;

  @OneToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "category_id", referencedColumnName = "id")
  private Category category;

  @OneToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "product_type_id", referencedColumnName = "id")
  private ProductType productType;

  @Column(name = "createTime")
  @Temporal(TemporalType.DATE)
  private Date createTime;

}
