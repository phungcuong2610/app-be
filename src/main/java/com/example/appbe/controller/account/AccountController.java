package com.example.appbe.controller.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.appbe.auth.JwtUtil;
import com.example.appbe.dto.ErrorRes;
import com.example.appbe.dto.LoginReq;
import com.example.appbe.dto.LoginRes;
import com.example.appbe.model.Account;
import com.example.appbe.services.account.AccountServiceAdaptee;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin
@RequestMapping("/account")
public class AccountController {
  @Autowired
  private AccountServiceAdaptee accountServiceAdaptee;

  private JwtUtil jwtUtil;

  public AccountController(AuthenticationManager authenticationManager, JwtUtil jwtUtil) {
    this.jwtUtil = jwtUtil;
  }

  @PostMapping(value = "/signin")
  public ResponseEntity<Object> SignIn(@RequestBody LoginReq loginReq) {
    // TODO: process POST request
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
    Account account = accountServiceAdaptee.getAccountByUsername(loginReq.getUsername());
    Boolean checkAddress = bCryptPasswordEncoder.matches(loginReq.getPassword(), account.getPassword());
    if (account != null && checkAddress) {
      String token = jwtUtil.createToken(account);
      LoginRes loginRes = new LoginRes(account.getId(), account.getUsername(), token, account.getRole());
      return new ResponseEntity<>(loginRes, HttpStatus.OK);
    } else {
      ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, "Invalid username or password");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
  }

  @PostMapping(value = "/signup")
  public ResponseEntity<Object> signUp(@RequestBody LoginReq loginReq) {
    // TODO: process POST request
    try {
      BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
      if (loginReq.getRole() == null) {
        Account account = new Account(null, loginReq.getUsername(),
            bCryptPasswordEncoder.encode(loginReq.getPassword()),
            "USER");

        return new ResponseEntity<>(accountServiceAdaptee.createAccount(account), HttpStatus.OK);
      } else {
        Account account = new Account(null, loginReq.getUsername(),
            bCryptPasswordEncoder.encode(loginReq.getPassword()),
            loginReq.getRole());
        return new ResponseEntity<>(accountServiceAdaptee.createAccount(account), HttpStatus.OK);
      }

    } catch (Exception e) {
      // TODO: handle exception
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

}
