package com.example.appbe.controller.product_detail;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.appbe.dto.UpdateProductDetailDto;
import com.example.appbe.model.ProductDetail;
import com.example.appbe.services.product.ProductService;
import com.example.appbe.services.product_detail.ProductDetailService;

@CrossOrigin()
@RestController
@RequestMapping("/product-detail")
public class ProductDetailController {
    @Autowired
    private ProductDetailService productDetailService;

    @Autowired
    private ProductService productService;

    @GetMapping("/product-detail-by-product-id")
    public ResponseEntity<Object> getProductDetailByProductId(@RequestParam("product_id") Integer productId) {
        return new ResponseEntity<>(productDetailService.getProductDetailByProductId(productId), HttpStatus.OK);
    }

    @DeleteMapping("/delete-product")
    public ResponseEntity<Object> deleteProduct(@RequestParam("id") Integer id) {
        productDetailService.deleteProduct(id);
        productService.deleteProductById(id);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @PostMapping("/update-product-detail")
    public ResponseEntity<Object> updateProductDetail(
            @RequestBody UpdateProductDetailDto updateProductDetailDto) {
        ProductDetail productDetail = productDetailService.updateProductDetail(updateProductDetailDto);
        return new ResponseEntity<>(productDetail, HttpStatus.OK);
    }

    @GetMapping(value = "/get-product-detail-by-pid")
    public ResponseEntity<Object> getProductByPid(@RequestParam("pid") Integer pid) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        List<List<Object>> tmp = productDetailService.getProductDetialByPid(pid);
        Map<String, Object> result = new HashMap<>();
        result.put("pid", tmp.get(0).get(0));
        result.put("pdid", tmp.get(0).get(1));
        result.put("create_time", formatter.format(tmp.get(0).get(2)));
        result.put("description", tmp.get(0).get(3));
        result.put("discount", tmp.get(0).get(4));
        result.put("price", tmp.get(0).get(5));
        result.put("product_image", tmp.get(0).get(6));
        result.put("product_name", tmp.get(0).get(7));
        result.put("quantity", tmp.get(0).get(8));
        result.put("sold", tmp.get(0).get(9));
        result.put("cpu", tmp.get(0).get(10));
        result.put("hdh", tmp.get(0).get(11));
        result.put("ocung", tmp.get(0).get(12));
        result.put("ram", tmp.get(0).get(13));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
