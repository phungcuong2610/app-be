package com.example.appbe.controller.demo;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.appbe.utils.FileUploadUtil;

@RestController
@CrossOrigin
@RequestMapping("/demo")
public class FileController {
  @GetMapping("")
  public String demo() {
    return "Hello";
  }

  @PostMapping("/uploadImage")
  public String savePhotos(@RequestParam("images") MultipartFile multipartFile) throws Exception {
    String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
    // long size = multipartFile.getSize();
    return "/downloadFile/";
  }
}
