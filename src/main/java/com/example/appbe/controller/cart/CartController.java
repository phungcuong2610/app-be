package com.example.appbe.controller.cart;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.appbe.dto.CartDto;
import com.example.appbe.dto.CartResponse;
import com.example.appbe.dto.IdsDto;
import com.example.appbe.dto.UpdateCartDto;
import com.example.appbe.model.Cart;
import com.example.appbe.model.Product;
import com.example.appbe.services.cart.CartService;
import com.example.appbe.services.product.ProductService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@CrossOrigin
@RestController
@RequestMapping("/cart")
public class CartController {
  @Autowired
  private CartService cartService;

  @Autowired
  private ProductService productService;

  @GetMapping(value = "/get-cart-by-account-id")
  public ResponseEntity<Object> getMethodName(@RequestParam String accountId) {
    List<Cart> carts = cartService.getCartByAccounId(Integer.parseInt(accountId), false);
    List<Integer> pIds = new ArrayList<>();
    List<Integer> cartIds = new ArrayList<>();
    carts.forEach(item -> {
      pIds.add(item.getProduct_id());
      cartIds.add(item.getId());
    });
    List<Product> products = productService.getProductByIds(pIds);

    return new ResponseEntity<>(new CartResponse(
        products.stream().sorted(Comparator.comparingInt(Product::getId)).collect(Collectors.toList()),
        carts.stream().sorted(Comparator.comparingInt(Cart::getProduct_id)).collect(Collectors.toList()), cartIds),
        HttpStatus.OK);
  }

  @DeleteMapping(value = "/delete-cart-ids")
  public ResponseEntity<Object> deleteCartByIds(@RequestBody IdsDto idsDto) {
    List<Integer> ids = idsDto.getIds();
    cartService.deleteCartByIds(ids);
    return new ResponseEntity<>(ids, HttpStatus.OK);
  }

  @PostMapping(value = "/add-cart")
  public ResponseEntity<Object> addCart(@RequestBody CartDto cardDto) {
    // TODO: process POST request
    Cart cart = cartService.createCart(cardDto);
    return new ResponseEntity<>(cart, HttpStatus.OK);
  }

  @PostMapping(value = "/update-cart")
  public ResponseEntity<Object> updateCart(@RequestBody UpdateCartDto updateCartDto) {
    // TODO: process POST request
    Cart cart = cartService.updateCart(updateCartDto);
    return new ResponseEntity<>(cart, HttpStatus.OK);
  }

  @DeleteMapping(value = "/delete-cart")
  public ResponseEntity<Object> deleteCart(@RequestParam("id") Integer id) {
    System.out.println(id);
    cartService.deleteCart(id);
    return new ResponseEntity<>("OK", HttpStatus.OK);
  }

}
