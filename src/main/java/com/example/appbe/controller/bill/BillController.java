package com.example.appbe.controller.bill;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.appbe.dto.BillResponse;
import com.example.appbe.dto.CreateBillDto;
import com.example.appbe.dto.HistoryResponse;
import com.example.appbe.model.Bill;
import com.example.appbe.model.Cart;
import com.example.appbe.model.Product;
import com.example.appbe.model.User;
import com.example.appbe.repositories.CartRepository;
import com.example.appbe.services.bill.BillService;
import com.example.appbe.services.cart.CartService;
import com.example.appbe.services.user.UserService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin
@RequestMapping("/bill")
public class BillController {
  @Autowired
  private BillService billService;

  @Autowired
  private UserService userService;

  @PostMapping(value = "/create-bill")
  public ResponseEntity<Object> postMethodName(@RequestBody CreateBillDto createBillDto) {
    // TODO: process POST request
    Bill bill = billService.createBill(createBillDto);
    User user = userService.getUserByAccountId(createBillDto.getAccountId());
    BillResponse billResponse = new BillResponse(user, bill);
    return new ResponseEntity<>(billResponse, HttpStatus.OK);
  }

  @GetMapping(value = "/get-bill-by-account")
  public ResponseEntity<Object> getMethodName(@RequestParam String accountId) {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    List<List<Object>> bills = billService.getBillByAccountId(Integer.parseInt(accountId));
    List<Object> result = bills.stream().map(item -> {
      Map<String, Object> map = Map.of("product_id", item.get(0), "product_image", item.get(1), "product_name",
          item.get(2), "quantity", item.get(3), "price", item.get(4), "create_time", formatter.format(item.get(6)));
      return map;
    }).collect(Collectors.toList());
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

}
