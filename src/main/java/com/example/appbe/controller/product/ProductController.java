package com.example.appbe.controller.product;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.appbe.dto.CreateProductDto;
import com.example.appbe.model.Product;
import com.example.appbe.services.product.ProductService;

@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {
  @Autowired
  private ProductService productService;

  @PostMapping("/create-product")
  public ResponseEntity<Object> createProduct(@ModelAttribute("createProduct") CreateProductDto createProductDto) {
    try {
      Product product = productService.createProduct(createProductDto);
      return new ResponseEntity<>(product, HttpStatus.OK);
    } catch (Exception e) {
      // TODO: handle exception
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("/product-by-filter")
  public ResponseEntity<Object> getProductByFilter(
      @RequestParam(name = "categoryId", required = false, defaultValue = "") Integer categoryId,
      @RequestParam(name = "productTypeId", required = false, defaultValue = "") Integer productTypeId) {
    List<Product> listProducts = productService.getProductByFilter(categoryId, productTypeId);
    return new ResponseEntity<>(listProducts, HttpStatus.OK);
  }

  @GetMapping("/product-by-name")
  public ResponseEntity<Object> getProductByName(
      @RequestParam(name = "productName", required = false, defaultValue = "") String productName) {
    List<Product> listProducts = productService.getProductsByName(productName);
    System.out.println(listProducts);
    return new ResponseEntity<>(listProducts, HttpStatus.OK);
  }

  @GetMapping("/get-all-product")
  public ResponseEntity<Object> getAllProduct() {
    return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
  }

}
