package com.example.appbe.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.appbe.dto.UpdateUserDto;
import com.example.appbe.model.User;
import com.example.appbe.services.user.UserService;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
  @Autowired
  private UserService userService;

  @PostMapping("/update-user")
  public ResponseEntity<Object> updateUser(@ModelAttribute("updateUser") UpdateUserDto updateUserDto) throws Exception {
    User user = userService.updateUser(updateUserDto);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @GetMapping("/user-by-accountId")
  public ResponseEntity<Object> getUserByAccountId(
      @RequestParam(name = "account_id", required = false) Integer accountId) {
    User user = userService.getUserByAccountId(accountId);
    if (user == null) {
      return new ResponseEntity<>("not infomation", HttpStatus.OK);
    }
    return new ResponseEntity<>(user, HttpStatus.OK);
  }
}
