package com.example.appbe.interfaces;

import com.example.appbe.dto.UpdateUserDto;
import com.example.appbe.model.User;

public interface UserInterface {
  User updateUser(UpdateUserDto updateUserDto) throws Exception;

  User getUserByAccountId(Integer accountId);
}
