package com.example.appbe.interfaces;

import com.example.appbe.model.History;

public interface HistoryInterface {
  public History createHistory(Integer account_id);

  public History getHistoryById(Integer id);
}
