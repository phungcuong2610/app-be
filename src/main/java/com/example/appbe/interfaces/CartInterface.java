package com.example.appbe.interfaces;

import java.util.List;

import com.example.appbe.dto.CartDto;
import com.example.appbe.dto.UpdateCartDto;
import com.example.appbe.model.Cart;

public interface CartInterface {
    Cart createCart(CartDto cardDto);

    Cart updateCart(UpdateCartDto updateCartDto);

    void deleteCart(Integer id);

    void deleteCartByIds(List<Integer> ids);

    List<Cart> getCartByIds(List<Integer> ids);

    Cart updateIsPay(Integer id, boolean isPay, Integer billId);

    List<Cart> getCartByAccounId(Integer accountId, boolean isPay);

    List<Cart> getCartByBillId(List<Integer> billIds);
}
