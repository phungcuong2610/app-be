package com.example.appbe.interfaces;

import java.util.List;

import com.example.appbe.model.Category;

public interface CategoryInterface {
    List<Category> getAllCategory();

    Category findCategoryById(Integer Id);
}
