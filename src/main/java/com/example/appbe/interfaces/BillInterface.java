package com.example.appbe.interfaces;

import java.util.List;

import com.example.appbe.dto.CreateBillDto;
import com.example.appbe.model.Bill;

public interface BillInterface {
  public Bill createBill(CreateBillDto createBillDto);

  public List<List<Object>> getBillByAccountId(Integer accountId);
}
