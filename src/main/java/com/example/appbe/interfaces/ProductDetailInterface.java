package com.example.appbe.interfaces;

import java.util.List;

import com.example.appbe.dto.UpdateProductDetailDto;
import com.example.appbe.model.ProductDetail;

public interface ProductDetailInterface {
    ProductDetail createProductDetail(Integer productId);

    ProductDetail updateProductDetail(UpdateProductDetailDto updateProductDetailDto);

    ProductDetail getProductDetailByProductId(Integer productId);

    List<List<Object>> getProductDetialByPid(Integer pid);

    void deleteProduct(Integer id);
}
