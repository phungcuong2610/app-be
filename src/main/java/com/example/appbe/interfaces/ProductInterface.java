package com.example.appbe.interfaces;

import java.util.List;
// import java.util.Optional;

import com.example.appbe.dto.CreateProductDto;
import com.example.appbe.dto.UpdateProductDto;
import com.example.appbe.model.Product;

public interface ProductInterface {
    Product createProduct(CreateProductDto createProductDto) throws Exception;

    List<Product> getAllProducts();

    List<Product> getProductByFilter(Integer categoryId, Integer productTypeId);

    List<Product> getProductsByName(String productName);

    List<Product> getProductByIds(List<Integer> ids);

    Product getProductById(Integer Id);

    Product updateProductById(UpdateProductDto updateProductDto);

    Product updateProductById1(Integer id, Integer soLuongBan);

    void deleteProductById(Integer Id);
}
