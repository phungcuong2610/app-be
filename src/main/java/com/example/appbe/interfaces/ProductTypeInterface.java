package com.example.appbe.interfaces;

import java.util.List;

import com.example.appbe.model.ProductType;

public interface ProductTypeInterface {
    List<ProductType> getAllProductTypes();

    ProductType findProductTypeById(Integer Id);
}
