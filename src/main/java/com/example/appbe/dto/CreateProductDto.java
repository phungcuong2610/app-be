package com.example.appbe.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class CreateProductDto {
    private MultipartFile productImage;
    private String productName;
    private Double price;
    private Integer quantity;
    private String description;
    private Double discount;
    private Integer categoryId;
    private Integer productTypeId;
}
