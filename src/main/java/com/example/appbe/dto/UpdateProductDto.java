package com.example.appbe.dto;

import lombok.Data;

@Data
public class UpdateProductDto {
    private Integer id;
    private String productName;
    private Double price;
    private Integer quantity;
    private String description;
    private Double discount;
    private Integer categoryId;
    private Integer productTypeId;
}
