package com.example.appbe.dto;

import lombok.Data;

@Data
public class UpdateCartDto {
    private Integer id;
    private Integer quantity;
    private Double price;
    private Boolean isPay;
}