package com.example.appbe.dto;

import java.util.List;

import lombok.Data;

@Data
public class CreateBillDto {
  List<Integer> cartIds;
  Integer accountId;
  Double total;
}
