package com.example.appbe.dto;

import lombok.Data;

@Data
public class UpdateProductDetailDto {
    private String cpu;
    private String ram;
    private String ocung;
    private String hdh;
    private Integer productId;
}
