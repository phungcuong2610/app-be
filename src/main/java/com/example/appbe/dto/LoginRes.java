package com.example.appbe.dto;

import lombok.Data;

@Data
public class LoginRes {
    private Integer id;
    private String username;
    private String token;
    private String role;

    public LoginRes() {
    }

    public LoginRes(Integer id, String username, String token, String role) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.role = role;
    }

}
