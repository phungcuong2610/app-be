package com.example.appbe.dto;

import java.util.List;

import lombok.Data;

@Data
public class IdsDto {
  private List<Integer> ids;
}
