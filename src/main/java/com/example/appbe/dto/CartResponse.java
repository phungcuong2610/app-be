package com.example.appbe.dto;

import java.util.List;

import com.example.appbe.model.Cart;
import com.example.appbe.model.Product;

public class CartResponse {
  List<Product> products;
  List<Cart> carts;
  List<Integer> pIds;

  public CartResponse(List<Product> products, List<Cart> carts, List<Integer> pIds) {
    this.products = products;
    this.carts = carts;
    this.pIds = pIds;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }

  public List<Cart> getCarts() {
    return carts;
  }

  public void setCarts(List<Cart> carts) {
    this.carts = carts;
  }

  public List<Integer> getpIds() {
    return pIds;
  }

  public void setpIds(List<Integer> pIds) {
    this.pIds = pIds;
  }

}
