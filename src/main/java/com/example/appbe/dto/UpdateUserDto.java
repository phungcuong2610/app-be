package com.example.appbe.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class UpdateUserDto {
  private MultipartFile avatar;
  private String fullname;
  private String phone;
  private String gmail;
  private String username;
}
