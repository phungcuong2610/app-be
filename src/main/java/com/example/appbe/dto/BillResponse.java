package com.example.appbe.dto;

import com.example.appbe.model.User;
import com.example.appbe.model.Bill;

public class BillResponse {
  private User user;
  private Bill bill;

  public BillResponse(User user, Bill bill) {
    this.user = user;
    this.bill = bill;
  }

  public BillResponse() {
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Bill getBill() {
    return bill;
  }

  public void setBill(Bill bill) {
    bill = bill;
  }

}
