package com.example.appbe.dto;

import lombok.Data;

@Data
public class CartDto {
    private Integer productId;
    private Integer accountId;
    private Integer quantity;
    private Double price;
    private Boolean isPay;
}
