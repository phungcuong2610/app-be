package com.example.appbe.dto;

import java.util.List;
import java.util.Map;

public class HistoryResponse {
  private List<Map<String, Object>> billDatas;
  private List<Map<String, Object>> historyPay;

  public HistoryResponse() {
  }

  public HistoryResponse(List<Map<String, Object>> billDatas, List<Map<String, Object>> historyPay) {
    this.billDatas = billDatas;
    this.historyPay = historyPay;
  }

  public List<Map<String, Object>> getBillDatas() {
    return billDatas;
  }

  public void setBillDatas(List<Map<String, Object>> billDatas) {
    this.billDatas = billDatas;
  }

  public List<Map<String, Object>> getHistoryPay() {
    return historyPay;
  }

  public void setHistoryPay(List<Map<String, Object>> historyPay) {
    this.historyPay = historyPay;
  }

}
