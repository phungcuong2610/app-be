package com.example.appbe.dto;

import lombok.Data;

@Data
public class ProductByFilterDto {
    private Integer categoryId;
    private Integer productTypeId;
}
