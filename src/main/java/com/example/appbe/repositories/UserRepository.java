package com.example.appbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.appbe.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "select * from users where account_id = :account_id", nativeQuery = true)
    User getUserByAccountId(@Param("account_id") Integer accountId);
}
