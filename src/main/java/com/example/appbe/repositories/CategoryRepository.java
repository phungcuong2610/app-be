package com.example.appbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.appbe.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
