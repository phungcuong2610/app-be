package com.example.appbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.appbe.model.History;

public interface HistoryRepository extends JpaRepository<History, Integer> {

}
