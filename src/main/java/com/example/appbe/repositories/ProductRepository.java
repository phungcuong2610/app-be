package com.example.appbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.appbe.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Query(value = "select * from products where category_id = :category_id or product_type_id = :product_type_id", nativeQuery = true)
    List<Product> getProductByFilter(@Param("category_id") Integer categoryId,
            @Param("product_type_id") Integer productTypeId);

    @Query(value = "Select * from products where product_name LIKE %:product_name%", nativeQuery = true)
    List<Product> getProductsByName(@Param("product_name") String productName);
}
