package com.example.appbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.appbe.model.ProductDetail;

public interface ProductDetailRepository extends JpaRepository<ProductDetail, Integer> {
    @Query(value = "select * from product_detail where product_id = :product_id", nativeQuery = true)
    ProductDetail getProductDetailByProductId(@Param("product_id") Integer productId);

    @Query(value = "Select p.id as pid, pd.id as pdid, p.create_time, p.description, p.discount, p.price, p.product_image, p.product_name, p.quantity, p.sold, pd.cpu, pd.hdh, pd.ocung, pd.ram from products as p left join product_detail as pd on p.id = pd.product_id where p.id = :pid", nativeQuery = true)
    List<List<Object>> getProductDetailByPid(@Param("pid") Integer pid);
}
