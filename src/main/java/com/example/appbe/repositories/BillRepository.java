package com.example.appbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.appbe.model.Bill;

public interface BillRepository extends JpaRepository<Bill, Integer> {
  @Query(value = "select * from bills where account_id = :account_id", nativeQuery = true)
  List<Bill> getBillAccountById(@Param("account_id") Integer accountId);

  @Query(value = "Select p.id, p.product_image, p.product_name, c.quantity, c.price, b.total, b.create_time from bills as b JOIN carts as c ON c.bill_id = b.id JOIN products as p ON p.id = c.product_id where c.is_pay = true and c.account_id = :account_id ", nativeQuery = true)
  List<List<Object>> getHistoryAccountById(@Param("account_id") Integer accountId);
}
