package com.example.appbe.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.appbe.model.Cart;

public interface CartRepository extends JpaRepository<Cart, Integer> {
  @Query(value = "Select * from carts where account_id = :account_id", nativeQuery = true)
  List<Cart> getAllCartByAccountId(@Param("account_id") Integer accountId);

  // @Modifying
  @Query(value = "Select * from carts where bill_id IN (:bill_id) and is_pay = true", nativeQuery = true)
  List<Cart> getCartsByBillId(@Param("bill_id") List<Integer> bill_id);

  @Modifying
  @Transactional
  @Query(value = "Delete from carts where id = :id", nativeQuery = true)
  void deletCart(@Param("id") Integer id);

}
