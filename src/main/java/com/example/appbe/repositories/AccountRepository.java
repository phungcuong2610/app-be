package com.example.appbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.appbe.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
  // @Transactional
  // @Modifying
  // @Query(value = "Select * from accounts where username = :username",
  // nativeQuery = true)
  // List<Account> findByUsername(@Param("username") String username);
  Account findByUsername(String username);

  // @Modifying
  // @Transactional
  // @Query(value = "insert into accounts(username, password, role)
  // values(:username, :password, :role);", nativeQuery = true)
  // Account createAccount(@Param("username") String username, @Param("password")
  // String password,
  // @Param("role") String role);
}
