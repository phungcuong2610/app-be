package com.example.appbe.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.appbe.auth.JwtAuthorizationFilter;
import com.example.appbe.services.account.AccountService;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    private AccountService accountService;
    private JwtAuthorizationFilter jwtAuthorizationFilter;

    public SecurityConfig(AccountService accountService, JwtAuthorizationFilter jwtAuthorizationFilter) {
        this.accountService = accountService;
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
    }

    // Cái này dùng để xác thực sau khi tiến hành tìm kiếm sử dụng Account Service
    @Bean
    AuthenticationManager authenticationManager(HttpSecurity httpSecurity, NoOpPasswordEncoder noOpPasswordEncoder)
            throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = httpSecurity
                .getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.userDetailsService(accountService).passwordEncoder(noOpPasswordEncoder);
        return authenticationManagerBuilder.build();
    }

    // Phân quyền
    @Bean
    SecurityFilterChain FilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf(csrf -> csrf.disable())
                .authorizeRequests(requests -> requests
                        .antMatchers("/account/**").permitAll()
                        .anyRequest().permitAll())
                .sessionManagement(management -> management.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                        .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class));
        return httpSecurity.build();
    }

    @SuppressWarnings("deprecation")
    @Bean
    NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
}
