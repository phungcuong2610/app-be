package com.example.appbe.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class ResourceConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // WSL
        registry.addResourceHandler("/images/**")
                .addResourceLocations("file:/home/leejaelee/app-be/images/");
        // Window
        // registry.addResourceHandler("/images/**")
        // .addResourceLocations("file:///C:/app-be/images/");
    }

}
